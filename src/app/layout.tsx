import "~/styles/globals.css";

import { Noto_Sans_HK } from "next/font/google";

const inter = Noto_Sans_HK({
  // subsets: ["latin"],
  preload: false,
  weight: ['300', '400', '500', '600', '700'],
  style: ['normal'],
  display: 'swap',
  variable: "--font-sans",
});

export const metadata = {
  title: "樓盤網站例子",
  description: "Property Listing Website Showcase",
  icons: [{ rel: "icon", url: "/pl-logo.png" }],
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`bg-slate-50 flex flex-col items-center font-sans ${inter.variable}`}>{children}</body>
    </html>
  );
}
