import Image from "next/image";
import Link from "next/link";

import FilterBar from "~/_components/FilterBar";
import SearchBar from "~/_components/SearchBar";
import { TagItem } from "~/_components/TagItem";

type SearchParams = {
  query?: string;
  page?: string;
  ptype?: string | Array<string>;
  locations?: string | Array<string>;
  tags?: string | Array<string>;
  min?: number;
  max?: number;
  sortBy?: "lowestPrice" | "highestPrice";
};

export const runtime = "edge";
export const dynamic = "force-dynamic";

export default function HomePage({
  searchParams,
}: {
  searchParams?: SearchParams;
}) {
  // console.log(searchParams);

  // const query = searchParams?.query ?? "";
  const currentPage = Number(searchParams?.page) ?? 1;
  return (
    <>
      <Header />

      <main className="my-16 mt-0 flex w-full flex-col items-center px-4 text-sm">
        <FilterBar />
        <ListingFeed searchParams={searchParams} currentPage={currentPage} />
      </main>

      <Footer />
    </>
  );
}

const Header = () => {
  return (
    <nav className="flex min-h-24 w-full max-w-7xl items-center justify-between px-4">
      <Link href="/" className="-ml-2 block font-bold lg:-ml-6">
        <Image src="/pl-logo.png" width="48" height="48" alt="site logo" />
      </Link>
      <SearchBar />
      <div>
        <UserIcon />
      </div>
    </nav>
  );
};

const Footer = () => {
  return (
    <footer className="flex min-h-64 w-full flex-col items-center gap-20 border-t-2 border-gray-100 bg-gray-100 px-4 pb-10 pt-20">
      <div className="flex w-full max-w-7xl md:justify-normal justify-center gap-20 text-gray-500 ">
        <Image
          src="/pl-logo.png"
          width="64"
          height="64"
          alt="site logo"
          className="lg:-lm-4 md:-ml-2 -ml-0"
        />
        
      </div>
      <div className="grid w-full max-w-7xl grid-cols-1 lg:grid-cols-3 gap-20 ">
        <div>
          <span className="mb-2 block font-medium text-center md:text-start">用戶支持</span>
          <ul className="flex flex-col gap-2 text-sm items-center md:items-start">
            <li>常見問題</li>
            <li>幫助中心</li>
            <li>用戶反饋</li>
            <li>社區論壇</li>
          </ul>
        </div>
        <div>
          <span className="mb-2 block font-medium text-center md:text-start">實用資訊</span>
          <ul className="flex flex-col gap-2 text-sm items-center md:items-start">
            <li>所有租盤</li>
            <li>所有賣盤</li>
            <li>網誌</li>
            <li>校網指南</li>
            <li>放盤指南</li>
          </ul>
        </div>
        <div>
          <span className="mb-2 block font-medium text-center md:text-start">租盤易</span>
          <ul className="flex flex-col gap-2 text-sm items-center md:items-start">
            <li>關於我們</li>
            <li>服務範圍</li>
            <li>加入我們</li>
            <li>聯絡方法</li>
          </ul>
        </div>
      </div>

      <div className="flex items-center md:items-start md:flex-row flex-col w-full max-w-7xl justify-between md:gap-20 gap-8 text-gray-500">
        <span>©{new Date().getFullYear()} property.pddn.net</span>
        <ul className="flex flex-row gap-10 text-sm items-center md:items-start">
          <li>私隱</li>
          <li>條款</li>
          <li>免責聲明</li>
        </ul>
      </div>
    </footer>
  );
};

type JSONResponse = {
  data?: Array<Property>;
  errors?: Array<{ message: string }>;
};

type Property = {
  id: number;
  attributes: {
    title: string;
    slug: string;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    tags?: { data: Array<{ id: string; attributes: { title: string } }> };
    locations?: { data: Array<{ id: string; attributes: { name: string } }> };
    rooms: number;
    toilets: number;
    saleableArea: number;
    price: number;
    featuredImage: {
      data: { id: string; attributes: { formats: { large: { url: string } } } };
    };
  };
};

const convertPtype = (str: string) => {
  switch (str) {
    case "獨立屋": {
      return "house";
      break;
    }
    case "村屋": {
      return "nthouse";
      break;
    }
    case "唐樓": {
      return "canton";
      break;
    }
    case "私人屋苑": {
      return "estate";
      break;
    }
    case "單幢樓": {
      return "single";
      break;
    }
    case "居屋": {
      return "hos";
      break;
    }
  }
  return "";
};

const ListingFeed = async ({
  searchParams,
  currentPage,
}: {
  searchParams?: SearchParams;
  currentPage?: number;
}) => {
  const filterSearchParams = new URLSearchParams();

  filterSearchParams.append("populate", "*");

  if (searchParams?.query) {
    filterSearchParams.append(
      "filters[$or][0][title][$contains]",
      searchParams.query,
    );
    filterSearchParams.append(
      "filters[$or][1][locations][name][$contains]",
      searchParams.query,
    );
    filterSearchParams.append(
      "filters[$or][2][tags][title][$contains]",
      searchParams.query,
    );
  }

  if (searchParams?.min) {
    filterSearchParams.append(
      "filters[price][$gte]",
      searchParams.min.toString(),
    );
  }

  if (searchParams?.max) {
    filterSearchParams.append(
      "filters[price][$lte]",
      searchParams.max.toString(),
    );
  }

  if (searchParams?.sortBy === "lowestPrice") {
    filterSearchParams.append("sort", "price:asc");
  } else if (searchParams?.sortBy === "highestPrice") {
    filterSearchParams.append("sort", "price:desc");
  }

  if (searchParams?.locations) {
    const loc = searchParams?.locations;
    if (typeof loc === "string") {
      filterSearchParams.append(
        "filters[locations][parent_locations][name][$eq]",
        loc,
      );
    } else {
      loc.forEach((l, i) => {
        filterSearchParams.append(
          `filters[$or][${i}][locations][parent_locations][name][$eq]`,
          l,
        );
      });
    }
  }

  if (searchParams?.ptype) {
    const ptype = searchParams?.ptype;

    if (typeof ptype === "string") {
      filterSearchParams.append("filters[ptype][$eq]", convertPtype(ptype));
    } else {
      ptype.forEach((t, i) => {
        filterSearchParams.append(
          `filters[$or][${i}][ptype][$eq]`,
          convertPtype(t),
        );
      });
    }
  }

  const apiEndpoint =
    "https://propertycms.pddn.net/api/properties?" +
    filterSearchParams.toString();

  console.log(apiEndpoint);

  const response = await fetch(apiEndpoint, {
    headers: {
      Authorization:
        "Bearer 61d6c665447cb288590cf8e7d337a9838760d3a159ab076b0c9f5bc3785674cda43bbf05e5ebca89675ccbe5a2616a758ef7059c217f960dd1bfc6e3783de0428d4ffefe8a1e7e3c2bab8077d8bfd9900f0881bd00617100c6184c26c4c96d76f30dcdf1a772b4e421c3d48c31f71023eb1749f5b4d01b220e037b241f0d8f64",
    },
  });

  const { data, errors } = (await response.json()) as JSONResponse;

  if (errors) return <></>;

  // console.log(JSON.stringify(data));

  return (
    <div className="grid w-full max-w-7xl grid-cols-1 gap-8 md:grid-cols-2 lg:grid-cols-4">
      {data &&
        [...data, ...data, ...data]?.map((p) => (
          <PropertyCard property={p} key={p.id} />
        ))}
    </div>
  );
};

const PropertyCard = ({ property }: { property: Property }) => {
  return (
    <div className="">
      <div className="relative">
        <Image
          className="h-64 w-full rounded-lg object-cover"
          src={`https://propertycms.pddn.net${property.attributes.featuredImage.data.attributes.formats.large.url}`}
          alt="Property preview"
          width="200"
          height="100"
        />
        <ul className="absolute bottom-2 flex w-full justify-end gap-2 px-2 text-xs">
          {property?.attributes.tags?.data.map((t) => (
            <TagItem key={t.id}>{t.attributes.title}</TagItem>
          ))}
        </ul>
      </div>

      <div className="mt-2 flex flex-col gap-2">
        <div className="flex flex-row justify-between">
          <h3 className="font-medium">
            #{property.id} {property.attributes.title}
          </h3>
          <span>
            {property.attributes.locations?.data.at(0)?.attributes.name}
          </span>
        </div>

        <div className="flex flex-row justify-between text-base">
          <span>${property.attributes.price}/mo</span>
          <span>{property.attributes.saleableArea} sqft</span>
        </div>
      </div>
    </div>
  );
};

const UserIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      className="h-8 w-8 text-gray-400"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M17.982 18.725A7.488 7.488 0 0 0 12 15.75a7.488 7.488 0 0 0-5.982 2.975m11.963 0a9 9 0 1 0-11.963 0m11.963 0A8.966 8.966 0 0 1 12 21a8.966 8.966 0 0 1-5.982-2.275M15 9.75a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
      />
    </svg>
  );
};
