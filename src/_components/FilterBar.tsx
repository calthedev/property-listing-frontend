"use client";

import { usePathname, useSearchParams, useRouter } from "next/navigation";
import { TagItem } from "./TagItem";
import TagGroup from "./TagGroup";


const FilterBar = () => {
  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();

  function handleSearch(
    target: EventTarget & (HTMLInputElement | HTMLSelectElement),
  ) {
    const params = new URLSearchParams(searchParams);
    const value = target.value;

    if (value) {
      params.set(target.name, value);
    } else {
      params.delete(target.name);
    }

    void router.replace(`${pathname}?${params.toString()}`);
  }

  return (
    <nav className="mb-10 grid w-full max-w-7xl grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-10 border-b-2 border-t-2 border-gray-100 py-8">
      <div className="flex flex-col gap-4">
        <h2>類型</h2>
        <TagGroup tagType="ptype">
          <TagItem>獨立屋</TagItem>
          <TagItem>村屋</TagItem>
          <TagItem>唐樓</TagItem>
          <TagItem>私人屋苑</TagItem>
          <TagItem>單幢樓</TagItem>
          <TagItem>居屋</TagItem>
        </TagGroup>
      </div>

      <div className="flex flex-col gap-4">
        <h2>地點</h2>
        <TagGroup tagType="locations">
          <TagItem>香港島</TagItem>
          <TagItem>九龍</TagItem>
          <TagItem>新界</TagItem>
        </TagGroup>
      </div>

      <div className="flex flex-col gap-4">
        <h2>價錢</h2>
        <div className="flex flex-row gap-2">
          <input
            className="focus:shadow-outline w-1/2 appearance-none rounded-lg border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            type="text"
            name="min"
            placeholder="$1000"
            onChange={(e) => {
              handleSearch(e.target);
            }}
            defaultValue={searchParams.get("min")?.toString()}
          />
          <input
            className="focus:shadow-outline w-1/2 appearance-none rounded-lg border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
            type="text"
            name="max"
            placeholder="$100000"
            onChange={(e) => {
              handleSearch(e.target);
            }}
            defaultValue={searchParams.get("max")?.toString()}
          />
        </div>
      </div>

      <div className="flex flex-col gap-4">
        <h2>排序</h2>
        <select
          name="sortBy"
          className="bg-white flex-0 focus:shadow-outline block w-full appearance-none rounded-lg border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
          onChange={(e) => {
            handleSearch(e.target);
          }}
          defaultValue={searchParams.get("max")?.toString() ?? "default"}
        >
          <option value="">預設排序</option>
          <option value="lowestPrice">按最低價錢排序</option>
          <option value="highestPrice">按最高價錢排序</option>
        </select>
      </div>
    </nav>
  );
};

export default FilterBar;
