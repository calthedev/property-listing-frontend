'use client'

import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState, useTransition, type ReactNode } from "react";

export const TagItem = ({ children, type = 'tags' }: {type?: 'tags' | 'location' | 'ptype' , children: string }) => {

  const [isActive, setIsActive] = useState(false)

  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();

  useEffect(()=>{
    const params = new URLSearchParams(searchParams);
    if(params.has(type, children)){
      setIsActive(true)
    }else{
      setIsActive(false)
    }
  }, [type, children, searchParams])

  const handleClick = (tag: string) => {
    const params = new URLSearchParams(searchParams);
    
    if (isActive) {
      params.delete(type, tag);
    } else {
      params.append(type, tag);
    }

    setIsActive(!isActive)

    const newParams = new URLSearchParams(params);

    void router.replace(`${pathname}?${newParams.toString()}`)
  }
  return (
    <li className={`cursor-pointer select-none rounded-lg px-3 py-2 text-slate-600 ${isActive?'bg-slate-600 text-white':'bg-slate-200'}`}
    onClick={()=>handleClick(children?.toString())}
    >
      {children}
    </li>
  );
};