'use client'

import { usePathname, useSearchParams, useRouter } from "next/navigation";

export default function SearchBar() {

  const searchParams = useSearchParams();
  const pathname = usePathname();
  const router = useRouter();

  function handleSearch(term: string) {
    const params = new URLSearchParams(searchParams);

    if (term) {
      params.set('query', term);
    } else {
      params.delete('query');
    }

    router.replace(`${pathname}?${params.toString()}`)
  }
  
  return (
    <input
      className="focus:shadow-outline w-1/2 appearance-none rounded-lg border px-3 py-2 text-sm leading-tight text-gray-700 shadow focus:outline-none"
      type="text"
      placeholder="搜尋地點、屋苑名稱、或者標籤"
      onChange={(e) => {
        handleSearch(e.target.value);
      }}
      defaultValue={searchParams.get('query')?.toString()}
    />
  );
}