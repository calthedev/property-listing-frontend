import { Children, ReactElement, cloneElement, isValidElement, type ReactNode } from "react";

export default function TagGroup({children, tagType}: {children: ReactNode, tagType: 'tags' | 'locations' | 'ptype'}){
  const props = {type: tagType}
  const childrenWithAdjustedProps = Children.map(children, child =>{
    if(isValidElement(child)){
      return cloneElement(child, props)
    }else{
      return child
    }
  }
 );
  return <ul className="flex flex-row flex-wrap gap-4">{childrenWithAdjustedProps}</ul>

}